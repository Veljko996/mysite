from django.db import models

# Create your models here.


class ClickModel(models.Model):
    """
        Model that represents every row in Click tabel in SQLite
    """
    id_field = models.CharField(max_length=64, blank=True)
    timestamp = models.BigIntegerField()
    type_field = models.CharField(max_length=32, blank=True)
    campaign = models.IntegerField()
    banner = models.IntegerField()
    content_unit = models.IntegerField()
    network = models.IntegerField()
    browser = models.IntegerField()
    operating_system = models.IntegerField()
    country = models.IntegerField()
    state = models.IntegerField()
    city = models.IntegerField()
