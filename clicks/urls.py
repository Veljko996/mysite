from django.urls import path

from . import views

urlpatterns = [
    path('get_clicks/<int:campaign>', views.ListClicks.as_view()),
]
