from rest_framework import serializers
from clicks.models import ClickModel


class ClickSerializer(serializers.ModelSerializer):

    class Meta:
        model = ClickModel
        fields = ('id_field', 'timestamp', 'type_field', 'campaign', 'banner', 'content_unit', 'network', 'browser',
                  'operating_system', 'country', 'state', 'city')
