from django.shortcuts import render
from clicks.models import ClickModel
from clicks.serializers import ClickSerializer

from rest_framework import generics
from rest_framework.response import Response

# Create your views here.


class ListClicks(generics.ListAPIView):
    serializer_class = ClickSerializer
    pagination_class = None

    def get(self, request, campaign, *args, **kwargs):
        queryset = ClickModel.objects.filter(campaign=campaign)
        serializer = self.serializer_class(queryset, many=True)
        return Response({"Clicks": len(serializer.data)})
